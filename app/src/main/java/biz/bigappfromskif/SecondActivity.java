package biz.bigappfromskif;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * @author Stas
 * @since 13.02.17.
 */

public class SecondActivity extends AppCompatActivity {

    EditText etChooseEmail;
    EditText etSendMassage;
    Button btnSend;
    String names[];;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        etChooseEmail = (EditText) findViewById(R.id.et_choose_email);
        etSendMassage = (EditText) findViewById(R.id.et_send_massage);
        btnSend = (Button) findViewById(R.id.btn_send);


        btnSend.setOnClickListener(mOnClickListener);
    }

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_send:
                    etChooseEmail.getText().toString();
                    etSendMassage.getText().toString();



                    names = new String[] {etChooseEmail.getText().toString()};

                    final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                    emailIntent.setType("text/plain");
                    emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, names);
                    emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Hello There");
                    emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, etSendMassage.getText().toString());


                    emailIntent.setType("message/rfc822");

                    try {
                        startActivityForResult(Intent.createChooser(emailIntent,
                                "Send email using..."), 23);
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(SecondActivity.this,
                                "No email clients installed.",
                                Toast.LENGTH_SHORT).show();
                    }

                    /*Intent mIntent = new Intent(SecondActivity.this, ThirdActivity.class);
                    startActivityForResult(mIntent, 1);*/

            }
        }


/**/
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        Intent mIntent = new Intent(SecondActivity.this, ThirdActivity.class);
        startActivity(mIntent);
    }
}
