package biz.bigappfromskif;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MidActivity extends AppCompatActivity {

    Button btnShare;
    Button btnSendMail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        btnShare = (Button) findViewById(R.id.btn_share);
        btnSendMail = (Button) findViewById(R.id.btn_send_email);


        btnShare.setOnClickListener(mOnClickListener);
        btnSendMail.setOnClickListener(mOnClickListener);
    }


    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_share:
                    Intent mIntent = new Intent(MidActivity.this, ThirdActivity.class);
                    startActivity(mIntent);
                break;
                case R.id.btn_send_email:
                    Intent m2Intent = new Intent(MidActivity.this, ThirdActivity.class);
                    startActivity(m2Intent);
                    break;
            }
        }

    };
}
