package biz.bigappfromskif;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * @author Stas
 * @since 13.02.17.
 */

public class ThirdActivity extends AppCompatActivity{
    TextView tvCongratulations;
    Button btnSendAgain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        tvCongratulations = (TextView) findViewById(R.id.tv_congratulations);
        btnSendAgain = (Button) findViewById(R.id.btn_send_again);

        tvCongratulations.setText("Congratulation!");


        btnSendAgain.setOnClickListener(mOnClickListener);
    }



    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_send_again:


                    Intent mIntent = new Intent(ThirdActivity.this, MainActivity.class);
                    startActivity(mIntent);
                    break;
            }
        }
    };
}
